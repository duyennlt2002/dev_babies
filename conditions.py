# truth falsy

# bool true false
# int -1, 0, 2, ... #0: True, =0 False
#string 'xxx' True, '' (empty string) False

# bool(True) 
# bool(False)
# bool(-1)
# bool(0)

#condition
# ==, !=, >,<,,>=,<=,not, is, is not

#condition cooordination: and, or
a=1
if (a==0):
    print ('a is 0')
elif (a==1):
    print('a is 1')
else:
    print('a not in(0,1)')
####
def main():
    year = int(input("Nam san xuat: "))

    if (year>=15):
        print("thay the")
        return 

    if (10<=year <15):
        print("bao tri")
        return

    print("khong co de xuat")

# main()

# def main():
#     year = int(input("Nam san xuat: "))

#     print("thay the") if (year >=15) else None
     
#     print("bao tri") if (10<=year <15) else None
       

#     print("khong co de xuat")

# main()
